;"use strict";
var o = document;

function evt(el, ev, fct) {
	if(el.addEventListener){
		el.addEventListener(ev, fct, false);
	}else{
		el.attachEvent('on' + ev, fct);	// IE :(
	}			
}

(function(){
	var jeu = {
		_victoire : {
			'pierre'  : 'ciseaux',
			'feuille' : 'pierre',
			'ciseaux' : 'feuille'
		},

		raz: function(){
			//~ Suppression d'éventuelles class CSS issues d'un précédent choix
			[].forEach.call(o.querySelectorAll('.selection'), function(el) {
				el.setAttribute('class', 'selection');
			});

			//~ Suppression du résultat précédent
			o.getElementById('main').innerHTML = "";
			o.getElementById('main').setAttribute('class', '');

			o.getElementById('result').innerHTML = "";
		},

		launch: function(el){
			//~ Remise à zéro
			this.raz();

			//~ Récupération du choix utilisateur + affichage
			var choix_joueur = el.getAttribute('data-type');
			o.getElementById('selection_joueur').classList.add(choix_joueur);

			//~ Génération du choix adversaire
			var choix_adversaire = this.getAdversaireChoice();
			o.getElementById('selection_adversaire').classList.add(choix_adversaire);

			//~ On devine le résultat de la manche
			this.setResult(choix_joueur, choix_adversaire);

		},

		getAdversaireChoice: function(){
			var _keys 	 = Object.keys(this._victoire);
			var oc       = Math.floor(Math.random() * _keys.length);
			var ran_key  = _keys[oc];
			return this._victoire[ran_key];
		},

		setResult: function(joueur, adversaire){
			var cible = o.getElementById('main'), result, txt;
			
			//~ Egalité
			if(joueur === adversaire){
				result = 'troll';
				txt    = 'Egalité !';
			}else if(this._victoire[joueur] === adversaire){
				//~ Le joueur gagne
				result = 'victory';
				txt    = 'Victoire !';
			}else{
				//~ Le joueur perd
				result = 'loose';
				txt    = 'Perdu !';
			}

			cible.setAttribute('class', 'loader');
			o.getElementById('result').innerHTML = txt;
			gif.get(result, cible);

		}
	};

	var gif = {
		api_url : "http://api.giphy.com/v1/gifs/search?q=",
		api_key : "dc6zaTOxFJmzC",

		get : function(state, cible){
			var request = new XMLHttpRequest();
			request.open('GET', this.api_url + state + "&api_key="+this.api_key, true);

			request.onload = function() {
				if (request.status >= 200 && request.status < 400) {
					var d  = JSON.parse(request.responseText);
					var oc = Math.floor(Math.random() * 25);
					cible.innerHTML = "<iframe style='display:none;' src='"+d.data[oc].embed_url+"'></iframe>";
					setTimeout(function(){
						cible.childNodes[0].style.display = 'block';
					}, 1000);
				} 
			};
			request.send();
		}
	};

	var init = {
		load : function(){
			[].forEach.call(o.querySelectorAll('#header a.choix'), function(el) {
				evt(el, 'click', function(){
					jeu.launch(el);
					return false;
				});
			});
		}
	};


	init.load();
})();